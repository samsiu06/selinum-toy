import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.CommonUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

/**
 * Created by duongnapham on 2/12/15.
 */
public class OpenBrowser {

    private final Logger logger = Logger.getLogger(OpenBrowser.class);
    private CommonUtils commonUtils = new CommonUtils();
    private WebDriver driver;

    @Test
    public void openBrowserWithUrl(){
        try{
            driver = commonUtils.openChromeBrowser("https://pp-webapps.metro-sourcing.hk/NewIMSWeb/");
//            WebElement searchIconElement = driver.findElement(By.xpath("/html/body/div[2]/header/div/div[2]/div/nav/ul/li[9]/a"));
//            searchIconElement.click();
            WebElement emailId = driver.findElement(By.id("emailId"));

            emailId.sendKeys("gms_production");
            WebElement passwordId = driver.findElement(By.id("passwordId"));

            passwordId.sendKeys("metro");
            WebElement loginButton = driver.findElement(By.id("loginButton"));
            loginButton.click();
            WebElement loginAC = driver.findElement(By.id("headerForm:_t24"));
            String loginACStr = loginAC.getText().toLowerCase();
            System.out.println("Page Title: " + loginACStr);
            Assert.assertEquals(loginACStr, "gms_production");
//            driver.quit();
        }
        catch (Exception e){
            logger.error("testURLs: " + e.getMessage());
        }
    }

}
